FROM ushahidi/node-ci:node-10-gulp-4
WORKDIR /var/app

COPY package.json .
RUN npm-install-silent.sh
COPY . .