#!/bin/bash
if [ ! -f platform-client/.env ]; then

    git clone https://github.com/ushahidi/platform-client/

    echo "dev.Dockerfile" >> platform-client/.gitignore
    echo "docker-compose.yml" >> platform-client/.gitignore
    echo "entrypoint.sh" >> platform-client/.gitignore

    cp -f docker/dev.Dockerfile platform-client/dev.Dockerfile
    cp -f docker/docker-compose.yml platform-client/docker-compose.yml
    cp -f docker/entrypoint.sh platform-client/entrypoint.sh
    cp -f docker/.env platform-client/.env
fi

cd platform-client
docker-compose up --build

